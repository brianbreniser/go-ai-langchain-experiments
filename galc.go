package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/tmc/langchaingo/llms"
	"github.com/tmc/langchaingo/llms/ollama"
	"github.com/tmc/langchaingo/llms/openai"

	"github.com/akamensky/argparse"
)

var AI_CHOICES = "Choose an AI: 'llama2', 'mistral', 'yi', 'openai'"

func main() {
	// Create new parser object
	parser := argparse.NewParser("langchain", "Language Chain Go")
	ai := parser.String("a", "ai", &argparse.Options{Required: true, Help: AI_CHOICES})
	temp := parser.Float("t", "temperature", &argparse.Options{Required: false, Help: "Temperature"})
	err := parser.Parse(os.Args)
	if err != nil {
		// Prints on -h or --help, or if called incorretly
		fmt.Print(parser.Usage(err))
	}

	var llm llms.LLM
	if *ai == "llama2" {
		llm, err = ollama.New(ollama.WithModel("llama2"))
		if err != nil {
			log.Fatal(err)
		}
	} else if *ai == "mistral" {
		llm, err = ollama.New(ollama.WithModel("mistral"))
		if err != nil {
			log.Fatal(err)
		}
	} else if *ai == "yi" {
		llm, err = ollama.New(ollama.WithModel("yi"))
		if err != nil {
			log.Fatal(err)
		}
	} else if *ai == "openai" {
		llm, err = openai.New()
		if err != nil {
			log.Fatal(err)
		}
	} else {
		fmt.Println("Unknown AI")
		fmt.Print(AI_CHOICES)
		os.Exit(1)
	}

	fmt.Println("=======================")
	fmt.Println("Staring experiments...")
	fmt.Println("=======================")

	ctx := context.Background()
	completion, err := llm.Call(ctx, "Human: Who was the first person in space?\nAssistant:",
		llms.WithTemperature(*temp),
		llms.WithStreamingFunc(streamFunc),
	)
	if err != nil {
		log.Fatal(err)
	}

	_ = completion
}

func streamFunc(ctx context.Context, chunk []byte) error {
	fmt.Print(string(chunk))
	return nil
}

