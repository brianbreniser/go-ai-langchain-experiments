# go-ai-langchain-experiments
![galc](galc.png)

## Name
Go Ai LangChain experiments (GALC)

## Description
This is a repository for experiments with go ai langchain

I'm working on a locally running app that can use locally running AI, or talk to chatgpt. This gives us the options of asking sensetive questions (No not like that, more like work related things, or customers data, or security things we don't want to leak) with locally running private AI, and the simple things can be asked of chatgpt.

## Usage
For locally running AI you will need: [ollama](ollama.ai)
For Chatgpt you will need an api key: [chatgpt](platform.openai.com)
- You must have your api key in your environment variables as OPENAI_API_KEY

## Project status
This project is in development. It is not ready for use.

NO warranty
NO trademark
NO Copywrite

